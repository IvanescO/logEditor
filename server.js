const express = require("express");
const path = require("path");
const bodyParser = require ("body-parser");

const index = require("./routes/index");
const log = require("./routes/logs");

const port = 4444;
const app = express();

app.set("views", path.join(__dirname,"client/views"));
app.set("view engine", "ejs");
app.engine("html",require("ejs").renderFile);

app.use(express.static(path.join(__dirname,"client/app")));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use("/", index);
app.use("/api", log);

app.listen(port, ()=>{
    console.log("server started")
});
