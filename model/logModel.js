var mongoose = require('mongoose');
    Schema = mongoose.Schema;
var logSchema =  new Schema({
    application:{
        type: String,
        default: '',
        required: ' Please enter a application name',
        trim: true
    },
    created:{
        type:Date,
        default: Date.now
    },
    errorLevel: {
        type: Number,
        min: 1,
        max: 5,
        required: 'Please enter a errorLevel'
    },
    host: {
        type: String,
        default: '',
        required: ' Please enter a host name',
        trim: true
    },
    user: {
        type: String,
        default: '',
        required: ' Please enter a userId',
        trim: true
    },
    virtualPID: {
        type: String,
        default: '',
        required: ' Please enter a virtualPID',
        trim: true
    },
    info1:{
        type: String,
        default: '',
        trim: true
    },
    info2:{
        type: String,
        default: '',
        trim: true
    },
    info3:{
        type: String,
        default: '',
        trim: true
    },
    info4:{
        type: String,
        default: '',
        trim: true
    },
    info5:{
        type: String,
        default: '',
        trim: true
    },
    message:{
        type: String,
        default: '',
        trim: true
    }

});

mongoose.model('Logs', logSchema);
