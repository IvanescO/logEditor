angular.module('myApp').service('logService',['$http',function($http){
    this.newLog = {
            application: "",
            created:"",
            errorLevel:"",
            host:"",
            info1:"",
            info2:"",
            info3:"",
            info4:"",
            info5:"",
            user:"",
            virtualPID:"",
            message:""
        };
    this.filter = {
        listApp:[],
        listHost:[],
        errLvl: [{
            name: "1(Fatal)",
            select: false,
        },
        {
            name: "2(Error)",
            select: false,
        },
        {
            name: "3(Info)",
            select: false,
        },
        {
            name: "4(Warn)",
            select: false,
        },
        {
            name: "5(Debug)",
            select: false,
        }],
        dateFrom:"" ,
        dateTo: "",
        virtPid: "",
        userId: "",
        message: "",
    };
    this.visible ={
        appList: false,
        hostList: false,
        errLvl: false,
        fromDate: false,
        pid: false,
        user: false,
        message: false
    };
    const LOGS_URL = '/api/logs/';
    const LOG_URL = '/api/log/';
    this.getLogs = function (){
        return $http.get(LOGS_URL)
        .then(function(res){
            return res.data;
        },function(err){
            console.log(err);
        });
    };
    this.saveLog = function(newLog){
        newLog.created = Date.now();
        return $http.post(LOG_URL, newLog).then(function(res){
            return res.data;
                //console.log(vm.data);
        },function(err){
            console.log(err);
        });
    };
    this.updLog = function(newLog){
        return $http.put(LOG_URL+newLog._id, newLog).then(function(res){
            return res.data;
                //console.log(vm.data);
        },function(err){
            console.log(err);
        });
    };
    this.delLog = function(log){
        return $http.delete('/api/log/'+log._id ).then(function(res){
                return res.data;
        },function(err){
            console.log(err);
        });
    }


}])