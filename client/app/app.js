var app = angular.module('myApp', ['ui.materialize','chart.js']).
controller('logsController', ["$scope","$http",'logService', function ($scope, $http, logService) {
        var vm = this;
        console.log(logService);
        vm.action = "Add";
        vm.newLog = logService.newLog;
        vm.filterObj = logService.filter;
        vm.visibleObj = logService.visible; 
        logService.getLogs().then(function(data){
            vm.data = data;
            vm.data.map(function(log){
                return log.application;
            }).filter(onlyUnique).forEach(function(appName) {
                vm.filterObj.listApp.push({
                    name: appName,
                    select: false
                });
            });
            vm.data.map(function(log){
                return log.host;
            }).filter(onlyUnique).forEach(function(hostName, index) {
                vm.filterObj.listHost.push({
                    name: hostName,
                    select: false
                });
            });;

        });
        vm.AddLog = AddLog;
        vm.SaveLogs = SaveLogs;
        vm.EditLog = EditLog;
        vm.DeleteLog = DeleteLog;
        vm.filterData = filterData;
        vm.cleanAll = cleanAll;
        vm.buildCharts = buildCharts;

        function buildCharts(from, to){
            var dateFrom = new Date(from);
            var dateTo = new Date(to);
            vm.dates = {
                label:[],
                fullDate:[],
            };
            var testarar = [];
            vm.countLogOnDay = [
                testarar
            ];
            Date.prototype.withoutTime = function () {
                return new Date(this).setHours(0, 0, 0, 0);
            };
            var arrF = vm.data.filter((elem)=>{
                 var created = new Date(elem.created).withoutTime();
                 return (created >= from && created <= to);
            });
            console.log(arrF);
            
            while(dateFrom <= dateTo){
                
                var strDate = dateFrom.toLocaleString("en-us", { month: "short" })+" "+ dateFrom.getDate();
                vm.dates.label.push(strDate);
                vm.dates.fullDate.push(dateFrom.withoutTime());
                
                dateFrom = new Date(dateFrom.setDate(dateFrom.getDate()+1));
                
            };
            
            for(var i =0 ;i <  vm.dates.fullDate.length;i++){
                var count = 0;
                for(var j = 0;j < arrF.length; j++){
                    var created = new Date(arrF[j].created).withoutTime();
                    if(created == vm.dates.fullDate[i]){
                        count++;
                    };
                };
                testarar.push(count);
            }
            
        }
        function SaveLogs(){
            if(vm.action == "Add"){
                logService.saveLog(vm.newLog).then(function(log){
                    vm.data.push(log);
                });
            } 
            else {
                logService.updLog(vm.newLog).then(function(log){
                    var id = vm.data.findIndex( elem => elem._id === log._id);
                    vm.data[id] = log;
            });
            }
        }
        function filterData(arr){
        }
        function cleanAll(){
            for (var key in vm.visibleObj) {
                vm.visibleObj[key] = false;
            };
        }
        function AddLog(){
            vm.action = "Add";
            vm.newLog = undefined;
        }
        
        function EditLog(log){
            vm.action = "Edit";
            vm.newLog = Object.create(log);
            
        }

        function DeleteLog(log){
            logService.delLog(log).then(function(log){
                vm.data.splice(vm.data.indexOf(log),1);
            });
            
        }

        function onlyUnique(value, index, self) { 
            return self.indexOf(value) === index;
        };
        
        
     
    }]);