angular.module('myApp').filter('filterByObj', function() {
    return function(arr, filterObj, visibleObj) {
            if(visibleObj.appList){
               return filterByApp(arr, filterObj.listApp);
            }
           
            else return arr;
            
            
            function filterByApp(arr, listApp){
                var newArr = [];
                var selectedApps = listApp.filter( elem => {
                    return elem.select;
                });
                if(!selectedApps.length){
                    console.log('im here1')
                    return arr;
                }
                else {
                        for(var i = 0; i < selectedApps.length; i++){
                        for(var j = 0; j < arr.length; j++){
                            if(arr[j].application == selectedApps[i].name){
                                newArr.push(arr[j]);
                            }
                        };
                };
                return newArr;
                }          
            };
}}).
filter('filterByHost', function(){
    return function(arr, filterObj, visibleObj){
        if(visibleObj.hostList){
            return filterByHost(arr, filterObj.listHost);
        }
        else return arr;

        function filterByHost(arr, listHost){
                var newArr = [];
                var selectedHosts = listHost.filter( elem => {
                    return elem.select;
                });
                console.log(selectedHosts);
                if(!selectedHosts.length){
                    console.log('im here2')
                    return arr;
                }
                else{
                    for(var i = 0; i < selectedHosts.length; i++){
                    for(var j = 0; j < arr.length; j++){
                        if(arr[j].host == selectedHosts[i].name){
                            newArr.push(arr[j]);
                        }
                    };
                };
                return newArr;
                }
            }
    }
}).filter('filterByErr', function(){
    return function(arr, filterObj, visibleObj){
        var newArr;
        if(visibleObj.errLvl){
            return filterByErr(arr, filterObj.errLvl);
        }
        else return arr;

        function filterByErr(arr ,errLvl){
            var newArr = [];
            var selectedErr = errLvl.filter (elem =>{
                return elem.select;
            });
            if(!selectedErr.length){
                return arr;
            }
            else {
                for(var i = 0; i < selectedErr.length; i++){
                    for(var j = 0; j < arr.length; j++){
                        if(arr[j].errorLevel == selectedErr[i].name[0]){
                            newArr.push(arr[j]);
                        }
                    };
                };
                return newArr;
            }
        }
    }
}).filter('filterByDate', function(){
    return function(arr, filterObj, visibleObj){
        var newArr;
        if(filterObj.dateFrom && filterObj.dateTo){
            return filterByDate(arr, filterObj.dateFrom, filterObj.dateTo)
        }
        else return arr;

        function filterByDate(arr ,dateFrom, dateTo){
            
            var from = new Date(dateFrom).valueOf();
            var to = new Date(dateTo).valueOf();
             return arr.filter((elem)=>{
                 //console.log(new Date(elem.created).valueOf() > new Date(dateFrom).valueOf());
                 var created = new Date(elem.created).valueOf();
                 return (created >= from && created <= to);
                // return (Date(elem.created) >= Date(dateFrom) && Date(elem.created) <= Date(dateTo));
            });
             
            
            
        }
    }
}).filter('filterByPid', function(){
    return function(arr, filterObj, visibleObj){
        var newArr;
        if(visibleObj.pid){
            return filterByPid(arr, filterObj.virtPid);
        }
        else return arr;

        function filterByPid(arr ,pid){
            if(pid)
            return arr.filter(function(element) {
                return matchRuleShort(element.virtualPID, pid);
            });
            else return arr;
        }
        
    }
}).filter('filterByUser', function(){
    return function(arr, filterObj, visibleObj){
        var newArr;
        if(visibleObj.user){
            return filterByUser(arr, filterObj.userId);
        }
        else return arr;

        function filterByUser(arr ,user){
            if(user)
            return arr.filter(function(element) {
                console.log(element.user);
                return matchRuleShort(element.user, user);
            });
            else return arr;
        }
        
    }
}).filter('filterByMess', function(){
    return function(arr, filterObj, visibleObj){
        var newArr;
        if(visibleObj.message){
            return filterByMess(arr, filterObj.message);
        }
        else return arr;

        function filterByMess(arr ,message){
            if(message)
            return arr.filter(function(element) {
                return matchRuleShort(element.message, message);
            });
            else return arr;
        }
        
    }
})
function matchRuleShort(str, rule) {
    return new RegExp("^" +rule.replace("_","*").replace("%","*").split("*").join(".*") + "$").test(str);
};