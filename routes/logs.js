const express = require ("express");
const router = express.Router();
var mongoose = require('mongoose');
    mongoose.connect('mongodb://localhost/logs');
    require('../model/logModel.js');
var Logs = mongoose.model('Logs');
var _ = require('lodash');
/**
 * Get all logs
 */
router.get("/logs", (req, res)=>{
    Logs.find().exec(function(err, logs){
        if (err) {
            return res.status(400).send({
            message: err
            });
        }
        res.json(logs);
    })    
});
/**
 * delete log by id
 */
router.delete("/log/:id",(req, res)=>{
    var id =req.params.id;
    Logs.findByIdAndRemove(id, req.body, function(err){
        if(err){
            return res.status(400).send({
                message: err
            });
        };
        return res.send(200);
    });
})
/**
 * Udpate log
 */
router.put("/log/:id",(req ,res)=>{
   var id =req.params.id;
    Logs.findByIdAndUpdate(id , req.body,{new: true}, function(err, log){
        if(err){
            return res.status(400).send({
                message: err
            });
        };
        res.json(log);
    });
})
/**
 * Create log
 */
router.post("/log", (req, res)=>{
    console.log(req.body);
    var log = new Logs(req.body);
    log.save(function(err) {
    if (err) {
      return res.status(400).send({
        message:(err)
      });
    } else {
      res.jsonp(log);
    }
  });
});

module.exports = router;
